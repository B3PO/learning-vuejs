import { createApp } from 'vue'
import App from './App.vue'
import { library } from "@fortawesome/fontawesome-svg-core";
import { faPhone } from "@fortawesome/free-solid-svg-icons";
import { faVuejs, faAngular, faAws, faHtml5, faBitcoin, faBootstrap, faBots,faBtc, faCss3, faCss3Alt, faDev, faDigitalOcean, faDocker, faEthereum, faGit, faGitAlt, faGithub, faGithubAlt, faGitlab, faGitSquare, faGithubSquare, faGolang, faJediOrder, faJs, faJsSquare, faLinux, faUbuntu,faMarkdown, faMonero, faNode, faNodeJs,faNpm,faPiedPiper, faPiedPiperAlt, faPiedPiperHat, faPiedPiperSquare, faPython,
faRaspberryPi} from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import VueTyperPlugin from 'vue-typer'

library.add(faPhone);
library.add( faVuejs, faAngular, faAws, faHtml5, faBitcoin, faBootstrap, faBots,faBtc, faCss3, faCss3Alt, faDev, faDigitalOcean, faDocker, faEthereum, faGit, faGitAlt, faGithub, faGithubAlt, faGitlab, faGitSquare, faGithubSquare, faGolang, faJediOrder, faJs, faJsSquare, faLinux, faUbuntu,faMarkdown, faMonero, faNode, faNodeJs,faNpm,faPiedPiper, faPiedPiperAlt, faPiedPiperHat, faPiedPiperSquare, faPython, faRaspberryPi);

createApp(App)
    .use(VueTyperPlugin)
    .component("font-awesome-icon", FontAwesomeIcon)
    .mount('#app')
