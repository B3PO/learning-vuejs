import http from "../http-common";

class DemoDataService {
  getAll() {
    return http.get("/demo");
  }

  get(id) {
    return http.get(`/demo/${id}`);
  }

  create(data) {
    return http.post("/demo", data);
  }

  update(id, data) {
    return http.put(`/demo/${id}`, data);
  }

  delete(id) {
    return http.delete(`/demo/${id}`);
  }

  deleteAll() {
    return http.delete(`/demo`);
  }

  findByTitle(title) {
    return http.get(`/demo?title=${title}`);
  }
}

export default new DemoDataService();