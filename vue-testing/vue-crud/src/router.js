import { createWebHistory, createRouter } from "vue-router";

const routes =  [
  {
    path: "/",
    alias: "/demo",
    name: "demo",
    component: () => import("./components/DemoList")
  },
  {
    path: "/demo/:id",
    name: "demo-details",
    component: () => import("./components/Demo")
  },
  {
    path: "/add",
    name: "add",
    component: () => import("./components/AddDemo")
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;