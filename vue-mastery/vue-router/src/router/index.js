import { createRouter, createWebHistory } from "vue-router";
import EventList from "../views/EventList.vue";
import Details from "../views/event/Details.vue";
import Register from "../views/event/Register.vue";
import Edit from "../views/event/Edit.vue";
import Layout from "../views/event/Layout.vue";
import NotFound from "../views/NotFound.vue";
import NetworkError from "../views/NetworkError.vue";
import NProgress from "nprogress";
import EventService from '@/services/EventService.js';
import GStore from '@/store';

const routes = [
  {
    path: "/",
    name: "EventList",
    component: EventList,
    props: route => ({ page: parseInt(route.query.page) || 1})
  },
  {
    path: "/events/:id",
    name: "Layout",
    props: true,
    component: Layout,
    beforeEnter: to => {
         return EventService.getEvent(to.params.id)
            .then(response => {
                GStore.event = response.data
            })
            .catch(error => {
                if(error.reponse && error.repsone.status == 404){
                    return {
                    name: '404Resource',
                    params: {resource: 'event'}
                }
                } else {
                    return { name: 'NetworkError' }
                }
                
          })
    },
    children: [
      {
        path: '',
        name: 'Details',
        component: Details
      },
      {
        path: "register",
        name: "Register",
        component: Register
      },
      {
        path: "edit",
        name: "Edit",
        component: Edit
      }
    ]
  },
  {
    path: '/event/:afterEvent(.*)',
    redirect: to => {
      return {path: '/events/' + to.params.afterEvent}
    }
  },
  {
    path: "/:catchAll(.*)",
    name: "NotFound",
    component: NotFound
  },
  {
    path: '/404/:resource',
    name: '404Resource',
    component: NotFound,
    props: true
  },
  {
    path: '/network-error',
    name: 'NetworkError',
    component: NetworkError
  }
  
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
  scrollBehavior(){
    if (savedPosition){
      return savedPosition
    } else {
      return {top: 0}
    }
    
  }
});

router.beforeEach(() => {
  NProgress.start()
})

router.afterEach(() => {
  NProgress.done()
})

export default router;
