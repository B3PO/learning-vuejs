# vue-vuex

## Project setup
```
cd vue-vuex
```

## Install packages and dependencies
```
npm install
```

### Run Frontend - Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Run Backend - Install json-server

### Open a separate terminal and navigate to vue-vuex
```
npm i -g json-server
```

### Run Server
```
json-server --watch db.json
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
